// sticky
  $.fn.sticky = function () {
    var $this=(this); 
    var stickyTop = $($this).offset().top;
    $(window).scroll(function() {
      var windowTop = $(window).scrollTop();
      if (stickyTop < windowTop && $($this).parent().height() + $($this).parent().offset().top - $($this).height() > windowTop) {
        $($this).css('position', 'sticky');
      } else {
        $($this).css('position', 'relative');
      }
    });   
  }

